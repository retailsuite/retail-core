'use strict';

module.exports = {
  database:         'builder',
  host:             'localhost',
  user:             'dev',
  password:         '111111',
  max:              20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
};
