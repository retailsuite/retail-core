module.exports = {

  corsOptions: {
    origin: 'http://site.com',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  },
  UPLOADS_DIR: 'static/uploads'
};
