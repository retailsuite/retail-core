
/** Методы */

/**
 * Рендерим TemplateBlock и возвращаем
 */
function TemplateBlock($app) {
  return function templateBlock(moduleName, blockName, args) {
    args = args || [];


    if (!moduleName || !blockName) {
      // throw new Error(`templateBlocks params are incorrect.`);
      return `<section m-bind="pop-message">Module or Block name not passed.</section>`;
    }

    let mod = $app.getModule(moduleName);

    if (!mod) {
      return `<section m-bind="pop-message">Module "${moduleName}" not registered. Misstype?</section>`;
    }

    let _render = this.res.render;

    let render = function (view, opts, _cb) {
      return new Promise((resolve, reject) => {
        // let _opt = {...opts, ...opt, ...{async: true}};
        _render(view, opts, _cb || function (err, html) { 
          if (err) reject(err); resolve(html);
        });
      });
    };
    
    let blocks = mod.blocks(this.req, render),
        block = blocks[blockName];

    
    if (!block) {
      return `<section m-bind="pop-message">Block "${blockName}" not found in Module "${moduleName}"</section>`;
    }

    return block(...args);
  };
}



module.exports = {
  TemplateBlock
};
