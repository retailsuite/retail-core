process.env['DEBUG'] = 'retail|notice|template|db';
// process.env['DEBUG'] = 'retail|notice|template';

let express             = require('express'),
    path                  = require('path'),
    debug                 = require('debug')('retail'),
    app                   = express(),
    bp                    = require('body-parser'),
    session               = require('express-session'),
    compression           = require('compression'),
    cors                  = require('cors'),
    literalRender         = require('./render-middleware'),

    {BrowserWrapper}    = require('./browser-wrapper'),
    RetailModule          = require('./retail-module'),
    {Table}         = require('pg-entity'),
    {db}                    = require('pg-entity'),
    // {templateDirs}      = require('./find-template'),
    html                  = require('./html'),
    sql                   = require('./sql'),
    Context               = require('./context'),
    http                  = require('http'),
    WebSocket             = require('ws');


let options;
const _modules = [],
      // VIEW/COMPONENT DIRECTORY:
      viewDir = [process.cwd() + '/pages'];

let server = http.createServer(app);

let WebSocketServer = new WebSocket.Server({
  server,
  path: '/socket/connect'
});


/** app options */
try {
  debug(`Load app options: 
              "${path.join(process.cwd(), 'options')}"`);

  options = require(path.join(process.cwd(), 'options'));
}

catch (err) {
  debug('Failed to load options.');
  throw new Error(err);
}



/** GLOBALS - just here! */
const $global = global.$global = {};

global.debug = debug;
global.html  = html;
global.sql   = sql;

$global.RetailModule = RetailModule;
$global.Table = Table;
$global.db = db;


debug(' Start app from __dirname:', __dirname, ' \n and process.cwd()', process.cwd());


/** "common" app object available throw all plugged modules */
let $app = {

  Context,

  options,

  registerModule,

  /** Proxy для контроля над locals, в будущем можно будет убрать */
  locals: new Proxy(app.locals, {

    get(target, prop) {
      debug(`app.locals read: ${prop}`);

      return target[prop];
    },

    set(target, prop, value) {
      debug(`app.locals set: ${prop} ${value}`);
      target[prop] = value;

      return true;
    }

  }),

  get: function (...args) {
    app.get(...args);
  },

  post: function (...args) {
    app.post(...args);
  },

  all: function (...args) {
    app.all(...args);
  },

  use: function (...args) {
    app.use(...args);
  },

  // TODO wrap(...fnList)
  wrap: function (...fn) {
    BrowserWrapper.wrap(...fn);
  },

  publishMethod: function (...fn) {
    BrowserWrapper.wrap(...fn);
  },

  modulePath: `${__dirname}/modules/`,

  addModule: function (m) {
    _modules.push(m);
  },

  getModule: function (name) {
    return _modules.find(m => {
      return m.name.toUpperCase() === name.toUpperCase();
    });
  },

  modules() {
    return _modules;
  },

  wss() {
    return WebSocketServer;
  }
};

global.$app = $app;


// TODO app locals example
$app.locals.APP_TITLE = 'RETAILFRONT';


if (options.corsOptions) app.use(cors(options.corsOptions));

/* TODO gzip -для легких запросов - неэффективен */
app.use(compression());

/** statics - использовать только для тестовой среды */
app.use('/static', express.static('static'));
app.use('/.build', express.static('.build'));

/** template literals templater */
app.engine('tpl.js', async function (filePath, props, callback) { // define the template engine
  let tpl;

  try { tpl = require(filePath); } catch (err) { if (err) return callback(err); }

  return callback(null, await tpl(props));
});

app.set('view engine', 'tpl.js'); // register the template engine
app.set('views', viewDir);

/** cookie & session */
app.set('trust proxy', 1); // trust first proxy
app.use(session({
  secret: 'Ill be back',
  resave: false,
  saveUninitialized: true,
  /* cookie: { secure: true } */
}));


/* parsing post */
app.use(bp.json({limit: '50mb'}));
app.use(bp.urlencoded({extended: true, limit: '50mb'}));


/* error handler */
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  debug(err);

  // render the error page
  res.status(err.status || 500);
  res.send(`<html>500 <br> <pre> ${err}  </pre> </html>`);
});

app.use(literalRender);

/* wrap functions to browser */
debug('handle "__browser" routes...');
app.all('/__browser/', BrowserWrapper.handle());
app.get('/__browser/wrap.js', BrowserWrapper.serve());
app.get('/__browser/list', BrowserWrapper.list());

/**
 *  registering modules. Роуты регистрируются в переданном порядке
 * (т.е. последний - будет перезаписывать методы предыдущего, при пересечении) 
 * @param {string} modules 
 */
function registerModule(...modules) {
  // let Mod; // exported module
  modules.map(Mod => {
    if (!Object.getPrototypeOf(Mod) == RetailModule) {
      throw new Error('Module must extend RetailModule');
    }

    let m = new Mod($app);

    /* Do something with modules,
     then add it to $app */
    $app.addModule(m);
  });
}

/** START APP */
$app.start = async function (opts) {
  const SITE_ID = opts.SITE_ID || 1;

  debug('SITE_ID = ', SITE_ID);


  /**
   * modules view
   * Если в модуле обозначен путь к директории view - viewDir - то добавляем его в express view path
   */

  let viewDir = app.get('views');

  for (let m of $app.modules()) {
    if (m.viewDir) viewDir.push(m.viewDir);
  }


  debug('view directories: ', viewDir.join(', '));

  app.set('views', viewDir);

  // await templateDirs(viewDir);

  // Загрузка контекста приложения
  let ctx = await Context(SITE_ID);

  server.listen(opts, function () {
    debug(`Listen on ${opts.host}:${opts.port}`);
  });
};


module.exports = $app;
