
class BrowserWrapper {
  constructor() {
    this.sharedFn = new Map();
    this.__path = '__browser';
    this.bodyWrapper = false;
  }

  log() {

  }

  /**
   * Создаем обертки для переданного массива функций, либо для собственных методов объекта 
   * @param {Array<Function>} fn or @param {Object} fn 
   */
  wrap(...fn) {
    if (!fn.length) {
      throw new Error('Not fn passed');
    }

    if (fn.length = 1) {
      if (typeof fn[0] === 'object') {
        fn = Object.values(fn[0]);
      }
    };

    fn.map(f => {
      if (typeof f !== 'function') {
        throw new Error('wrap: fn not a function');
      }

      this.sharedFn.set(f.name, f);
    });
  }

  /** 
   * generating browser js code
  */
  serve() {
    return (req, res) => {
      // if (this.bodyWrapper) return res.send(this.bodyWrapper);

      let body = '';

      body += ` let path = '/${this.__path}/'; `;

      // TODO при error - не возвращать return
      body += `

      window.$m = window.$m || {};
      $m.api = $m.api || {};
      window['__templateBlockLoader'] = (new function () {

        var __range = document.createRange();
        var __subscribers = [];
      
        this.append = function (buid, props) {


          if (!buid) {
            console.warn('buid not passed!');
            return;
          }
      
          var container = document.getElementById(buid);
      
          $m.api.callDynamicBlock(buid, props).then(function (html) {
      
            let frag = __range.createContextualFragment(html);
      
      
            container.innerHTML = '';
            container.appendChild(frag);

            __subscribers.map( function(fn) {
              fn(buid, container);
            });
      
          });
        };
      
        this.onLoad = function (fn) {
          __subscribers.push(fn);
        };
      
      }());

            /** 
             * just a simple fetch wrapper 
            */
            var post = function(url, opts) {

                var data = {
                
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    credentials: 'same-origin',
                    body: JSON.stringify(opts)
                
                };
                return fetch(url, data)
                    .then(function (resp) {

                        if (!resp.ok) {
                          // TODO handle !resp.ok
                          throw new Error('wrapped fetch wrong');
                        }
                        
                        var contentType = resp.headers.get('content-type');

                        if (contentType && contentType.includes('application/json')) {
                          return resp.json();
                        };
                        
                        return resp.text();

                        // TODO type BLOB is not handled yet.

                    })
                    .catch(function(err){return err; }); //todo throw err
            }; `;


      this.sharedFn.forEach((fn, fnName) => {
        body += `

            /** 
             * Server function wrapper "${fnName}" 
            */
            $m.api['${fnName}'] = function ${fnName}(...args) {
                if ( arguments.length !== ${fn.length} ) {
                    console.warn(' Server function ${fnName}; \\n Expected arguments count is ${fn.length} ');
                }
                return post(path, {fnName: '${fnName}', args: args});
            };

            // was here 

            `;
      });
      
      // this row - > $m.api['${fnName}'].toString = function() { return ${fn.toString().trim()}; }

      this.bodyWrapper = body;

      return res.send(this.bodyWrapper);
    };
  }

  /** 
   * Handling request from "shared" functions 
  */
  handle() {
    return (req, res) => {
      if (!req || !res) {
        throw new Error('req or res not passed');
      }

      let fnName = req.body.fnName,
          args = req.body.args;

      this.sharedFn.forEach((fn, fName) => {
        if (fName == fnName) {
          (async function () {
            // TODO async перенести выше к req, res
            // TODO - try/catch здесь нужны

            let fResult = await fn.apply({req, res}, args);

            return res.json(fResult);
          }());
        }
      });

      // return res.json({message: `function ${fnName} not found`});
    };
  }

  list() {
    return (req, res) => {
      let actions = ``;

      this.sharedFn.forEach((fn, fName) => 
        actions += `<div class="f">
          <div class="f-name">${fName}</div>
          <pre><code class="javascript">${fn.toString().trim()}</code></pre>
        </div>
        `); 

      let page = `
      <html>
        <head>  
        <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.15.10/build/styles/default.min.css">
        <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.15.10/build/highlight.min.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
        </head>
        <body>
          ${actions}
        </body>
      </html>
      `;


      res.send(page);
    };
  }
}

exports.BrowserWrapper = new BrowserWrapper();
