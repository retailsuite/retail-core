
'use strict';

const db = require('./db');

async function Context() {
  // var ds = DataStore();
      
  let settings = await db.sql('select * from app_context()');

  return settings;
};

module.exports = Context;
