'use strict';

const uuid    = require('uuid/v1'),
      Actions = require('./actions'),
      RetailModule = require('./retail-module'),
      {callDynamicBlock} = require('./dynamic-blocks');
      

class Core extends RetailModule {
  init() {
    // this.viewDir = __dirname + '/views';
  }

  start($app) {
    let templateBlock = Actions.TemplateBlock($app);

    $app.wrap(templateBlock);
    $app.wrap(callDynamicBlock);
  }
}

module.exports = Core;
