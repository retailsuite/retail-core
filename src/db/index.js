'use strict';

const os = require('os');

// const notice  = require('debug')('retail:db'),
const notice  = require('debug')('db'),
      path        = require('path'),
      {Pool}    = require('pg');


let conn;
// example:
// conn = {
//         database:                'mydatabase',
//         host:                    'localhost',
//         user:                    'admin',
//         password:                '111111',
//         max:                     20,
//         idleTimeoutMillis:       30000,
//         connectionTimeoutMillis: 2000,
//       }

try {
  let connConf = 'connection';

  notice(`DB connection parameters: "${path.join(process.cwd(), connConf)}"`);
  conn = require(path.join(process.cwd(), connConf));
}

catch (err) {
  throw new Error(`Can't load DB connection parameters:`, err);
}



module.exports = {
  query,
  sql,
  getVal,
  orm
};


const pool = new Pool(conn);

pool.on('connect', (conn) => {
  conn.on('notice', (msg) => {
    notice(`\n  ${msg.message}, \n WHERE: ${msg.where} \n`);
  });
});

pool.on('error', (err, client) => {
  throw err;
});

/**
 * 
 * @param {String} text 
 * @param {Array} params 
 */
async function query(text, params) {
  try {
    // console.time(`Time execution: ${text}`);
    let resp = await pool.query(text, params);

    // console.timeEnd(`Time execution: ${text}`);
    return resp.rows;
  } 
  
  catch (err) {
    notice(err);

    return err;
  }
}


async function sql(text, params) {
  console.log('\n\n Query SQL: \n');
  console.log(text);
  console.log('\n Query Params: \n');
  console.log(params);
  try {
    // console.time(`Time execution: ${text}`);

    let resp = await pool.query(text, params);
    
    // console.timeEnd(`Time execution: ${text}`);

    return resp.rows[0];
  }
  catch (err) {
    throw err;
  }
}

async function getVal(text, params) {
  try {
    let resp = await pool.query(text, params);

    return resp;
  }
  catch (err) {
    throw err;
    // return err;
  }
}

async function orm(table, command, options) {
  let result = await sql(`select * from orm('${table}', '${command}', '${JSON.stringify(options)}')`);
  
  if (result.retval !== 0) {
    console.log('\n\n\n');
    throw Error(`ORM retval = ${result.retval},\n ${result.res.error}`);
  }
  
  if (result.res) {
    return result.res;
  }


  debug(`__ ORM: EMPTY RESULT RETURNED: ${JSON.stringify(result)}`);

  return [];
}
