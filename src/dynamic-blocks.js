'use strict';

const uuid = require('uuid/v1');

/**
 *  метод вызываемый клиентом, возвращает html блок
 */
async function callDynamicBlock(buid, props) {
  /**
   * Для универсальности интерфейса работы с параметрами
   * сохраняем переданные с клиента параметры в объект req.query
   * 
   */
  Object.assign(this.req.query, props);

  let result,
      block = activeBlocks.get(buid); // TODO 'pop' or delete block after get

  if (!block) return `<!-- NO DYNAMIC BLOCK FOUND WITH ID ${buid} -->`;

  if (block instanceof Promise) {
    result = await block;
  }
  
  if (block instanceof Function) {
    let _render = this.res.render; // TODO .bind(this.res)?

    let render = function (view, opts, _cb) {
      return new Promise((resolve, reject) => {
        // let _opt = {...opts, ...opt, ...{async: true}};
        _render(view, opts, _cb || function (err, html) { 
          if (err) reject(err); resolve(html);
        });
      });
    };

    // вызываем обработчик блока, транслируем в него текущий инстанс реквеста
    result = await block.apply(this, [{req: this.req, res: this.res, props}]);
  }

  return result;
}

/**
 * Хранилище dynamic блоков
 */
let activeBlocks = (function activeBlocks() {
  let _blocks = new Map();

  return {

    add(buid, block) {
      _blocks.set(buid, block);
    },

    get(buid) {
      return _blocks.get(buid);
    },

    list() { return _blocks; }
  };
}());

//
function blockWrapper(buid) {
  return `<div class="__dynamic-template-block" id="${buid}" data-block-id="${buid}"></div>

  <script>

  (function(){

    var call = function() {
      __templateBlockLoader.append("${buid}");
    };
    
    if (typeof callDynamicBlock === "function") { 
      call(); 
    } 

    else {
      document.addEventListener('DOMContentLoaded', call);
    }

  }());

  </script>`;
};


/** 
 * DynamicBlock(fn, opt?)
 * return script anchor to template
 * Register async block with Promise (NOOOO PRE-CALLED) wrap
 * 
 * TODO: opt - DynamicBlock behavior options
 *  load: immediate, screen, delay(ms), 'scroll'
 *  
*/
function DynamicBlock(fn, opt) {
  return function (initialProps = {}) {
    let buid = uuid();

    // create promise-wrapper for functions
    let p = function ({req, res, props = {}}) {
      // fn = fn.bind(this);

      // fixme
      // сейчас - если переданы параметры с клиента - то перезаписываем начальные 
      // необходимо использовать named params
      props = {...initialProps, ...props};

      return new Promise((resolve, reject) => {
        try {
          // пропускаем инстанс входящего реквеста до темлейт_блока
          resolve(fn.apply(this, [{req, res, props}]));
        }
        catch (e) {
          reject(new Error(`Error calling fn(..args) in AsyncBlock:\n` + e));
        }
      });
    };

    // add block to stack with pre-called Promise
    activeBlocks.add(buid, p);

    return blockWrapper(buid);
  };
}

module.exports = {
  DynamicBlock,
  callDynamicBlock
};
