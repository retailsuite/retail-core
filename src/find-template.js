'use strict';

const fs   = require('fs');
const path = require('path');

let templateList = new Set();
//     tpl;

// function findTemplate(...templates) {

//   for (let i = 0; i < templates.length; i++) {
//     if (templateList.has(templates[i])) {
//       tpl = templates[i];
//       break; 
//     }
//   };

//   return tpl;
// }

async function templateDirs(dirs) {
  if (!Array.isArray(dirs) || dirs.length < 1)
    throw new Error('View directories is not array. Are you sure?');

  for (let dir of dirs) {
    (function readDir(dir, prev = '', filter = 'ejs') {
      for (let fileName of fs.readdirSync(dir)) {
        const fullPath = path.join(dir, fileName),
              prevPath = path.join(prev, fileName);

        if (fs.statSync(fullPath).isDirectory()) {
          readDir(fullPath, prevPath, filter);
        } 
        else if (prevPath.indexOf(filter) >= 0) {
          templateList.add(prevPath);
        } 
      }
    }(dir));
  }
}

module.exports = {templateDirs};
