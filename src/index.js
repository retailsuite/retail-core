'use strict';

const RetailModule = require('./retail-module') ;
const DynamicBlock = require('./dynamic-blocks');

const App = require('./app'),
      core = require('./core');

App.registerModule(core);

module.exports = {App, RetailModule, DynamicBlock};
