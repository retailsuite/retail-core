module.exports = async function (req, res, next)  {
  const render = res.render;

  /**
      * rendering with "layout" 
      */
  res.view = async function (view, props = {}, clb) {
    const query = {...req.body, ...req.params, ...req.query};

    return res.render(view, {req, res, query, props}, clb);
  };

  next();
};
