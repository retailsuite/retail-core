const uuid = require('uuid/v1');
const EventEmitter = require('events');


class RetailModule {
  constructor($app) {
    this.instance   = uuid();
    this.viewDir    = ''; //
    this.name       = this.constructor.name;
    this.models     = {};
    this.blocks     = function () {return {};};
    this.emitter = new EventEmitter(); 
    this.init();
    
    setTimeout (()=>{
      this.start($app);
    }, 200);

    debug(`Module '${this.name}' loaded.`);
  }

  /** noop */
  init() {
    debug(`No init's for module '${this.name}'. Skip.`);
  };

  /**
   * 
   * @param {app} application instance  
   */
  start($app) {
    debug(`You have to place some module code here. ${this.name}`);
  }

  templateBlocks(...args) {
    return this.blocks(...args);
  }

  on(event, handler) {
    this.emitter.on(event, handler);
  };
  
  emit(event, ...args) {
    this.emitter.emit(event, ...args);
  };
};

module.exports = RetailModule;
