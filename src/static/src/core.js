'use strict';

import {log, notice, warn} from '../vendors';
import {registerEvents} from './events';



notice('core.js imported.');


let handlers = [],
    subscribers = [],
    range = document.createRange();

/**
 * Получаем html код блок-враппера, создаем и возвращаем docFrag
 * @param {string} moduleName
 * @param {string} blockName
 * @param {Array} args
 */
async function getTemplateBlock(moduleName, blockName, args) {
  let x = await templateBlock(moduleName, blockName, args),
      wrapper = range.createContextualFragment(x),
      dynamic = wrapper.querySelector('.__dynamic-template-block'),
      id, node;

  if (dynamic) {
    // если DynamicTemplateBlock
    id = dynamic.dataset['blockId'];
  } 
    
  else {
    // если "обычный" TemplateBlock
    node = document.createElement('section');
    node.appendChild(wrapper);
  }

  // Promisify
  function load() {
    return new Promise((resolve) => {
      if (node) {
        log(`Looks like TemplateBlock is not Dynamic. Resolve Node.`);

        return resolve(node);
      }
      let fn = function (frag) {
        log(`getTemplateBlock("${moduleName}","${blockName}") resolved.`);
        resolve(frag);
      };

      subscribers.push({id, fn});
    });
  }

  return {
    wrapper: function () {
      if (!node) return wrapper;
      throw new Error(`Block "${moduleName}","${blockName}" is not DynamicTemplateBlock. Do not use wrapper. Or check network connection.`);
    },

    id: function () {
      if (id) return id;
      throw new Error(`Block ID is undefined for getTemplateBlock("${moduleName}","${blockName}")`);
    },
    load
  };
}

/**
 * onLoad - использовать только в одном месте, больне нигде вызывать onLoad не желательно
 * при необходимости - сделать обертку над этим onLoad и вызывать ее.
 */
__templateBlockLoader.onLoad(function (buid, blockElem) {
  log(`TemplateBlock "${buid}" loaded.`);

  mFetch(blockElem);

  subscribers.map(s => {
    // TODO необходимо реализовать 'once'?
    if (s.id === buid) s.fn(blockElem);
  });
});

/** Собирает все "data-bind" в стек для заданного узла (подразумевается это "страница")
 * @param {HtmlNode}
 * @return {Array} Element bindings array [ {elem, bind, template} ]
 **/
function mFetch(node) {
  // если нода не задана явно - то анализируем документ целиком
  if (!node) {
    throw new Error('Node not passed.');
  }

  let nodeList = node.querySelectorAll('[m-bind]');

  if (nodeList.lenght === 0) return;

  nodeList.forEach(function (n) {
    // Bind to
    let bindName = n.getAttribute('m-bind');

    let elem = handlers.find(el => {
      if (el.name === bindName) return true;
    });

    if (elem) {
      log(`Handler for m-bind "${bindName}" called.`);
      elem.handler(n);
    } else {
      warn(`Handler for m-bind "${bindName}" not found!`);
    }
    // to support client templating:
    // tpl = Hb.compile(n.innerHTML);
  });
}

/**
 * Функция хандлер для dom-узла (m-bind)
 * @param {string} bindName 
 * @param {function} fn 
 */
function mBind(bindName, fn) {
  if (handlers.find(b=>{return b.name === bindName;})) {
    warn(`mBind(${bindName}) already binded.`);

    return;
  }

  handlers.push({
    name: bindName,
    handler: fn
  });
}


class ElementHandler {
  /**
     * @param {HTMLElement} element
     * @param {Function}  handler
     */
  constructor(element, handler) {
    this.node = element; // проверить правильность элемента

    this.handler = (...args) => {
      handler.apply(this, args);

      return this;
    };

    this.init();

    this.field = {aaa: 1000};
  }

  init() {
    let events = ['onclick', 'onblur', 'onkeypress', 'onkeydown', 'onkeyup', 'onchange'];

    this.handler.apply(this, []); // pass this to handler fn
    registerEvents.apply(this, events);
  }
};

/**
 * По идентификатору блока, загружаем его повторно
 * @param {string} buid 
 */
async function reload(buid, props) {
  return new Promise((resolve) => {
    let fn = function (frag) {
      resolve({buid, frag});
    };

    subscribers.push({id: buid, fn});

    __templateBlockLoader.append(buid, props);
  });
}

function getBuid(node) {
  let container; 
    
  if (node.classList.contains('__dynamic-template-block')) {
    container = node;
  } else {
    container = node.closest('.__dynamic-template-block');
  }
  if (!container) {
    warn(` "__dynami-template-block" not found for node `, node);

    return null;
  }

  let buid = container.dataset['blockId'];

  if (!buid) 
    throw new Error(`Dynamic block conteiner found. But BlockId not exist.`);

  return buid;
}

/**
 * Находим родительский блок элемента, получаем его id и вызываем reload
 * @param {HTMLElement} node 
 */
function reloadBlock(node, props) {
  let buid = getBuid(node);

  return reload(buid, props);
}

function lazyReloadBlock(node) {
  let buid = getBuid(node);

  return function (props) {
    reload(buid, props);
  };
}

export {
  getTemplateBlock,
  ElementHandler,
  mFetch,
  mBind,
  reloadBlock,
  lazyReloadBlock
};
