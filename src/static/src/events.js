'use strict';

import { } from '../vendors';


function registerEvents(...events) {
  events.map(event => {
    let nodes = [];

    if (!this.node instanceof DocumentFragment) {
      if (this.node.hasAttribute(event)) {
        nodes.push(this.node);
      }
    }

    nodes.push(...this.node.querySelectorAll(`[${event}]`));

    nodes.forEach(n => {
      n[event] = n[event].bind(this);
    });
  });
}

export  {
  registerEvents
};
