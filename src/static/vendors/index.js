/** Подключение модулей и реекспорт в приложение здесь */
import debug      from './debug';

// set debug options
global.localStorage.debug = '*';
var log     = debug('log');
var warn    = debug('warn');
var notice  = debug('notice');


export {log, warn, notice};
