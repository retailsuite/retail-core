/*

*/

function response(view) {
  return function (req, res, next) {
    // собираем параметры Request в один объект и передаем в шаблон
    // TODO это необходимо добавить в res.view в core
    let query = {...req.body, ...req.params, ...req.query};

    res.view(view, {query});
  };
}

module.exports = {
  response
};
